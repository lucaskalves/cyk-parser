<?php

/**
 * Copyright (c) 2012 Adolfo Schneider, Lucas Kreutz, Rodrigo Zanella
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

class Node {
	protected $content;
	protected $probability = null;
	protected $left = null;
	protected $right = null;
	
	public function __construct($content) {
		$this->setContent($content);
	}
	
	public function setContent($content) {
		$this->content = $content;
	}
	
	public function getContent() {
		return $this->content;
	}
	
	public function setProbability($prob) {
		if(is_numeric($prob)) {
			$this->probability = $prob;
		} else {
			throw new Exception("Wrong probability given.");
		}
	}
	
	public function getProbability() {
		return $this->probability;
	}
	
	public function setLeftChild($node) {
		$this->left = $node;
	}
	
	public function getLeftChild() {
		return $this->left;
	}
	
	public function setRightChild($node) {
		$this->right = $node;
	}

	public function getRightChild() {
		return $this->right;
	}
	
	public function hasLeftChild() {
		return $this->left !== null;
	}
	
	public function hasRightChild() {
		return $this->right !== null;
	}
}
